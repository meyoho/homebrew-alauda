class ConsoleCli < Formula
  require_relative "../shared/bitbucket_download_strategy.rb"
  require "FileUtils"

  desc "📦🚀 Alauda Console Cli"
  homepage "https://bitbucket.org/mathildetech/alauda-console"
  version "2.11.0"

  if OS.mac?
    PLATFORM = "darwin"
  elsif OS.linux?
    PLATFORM = "linux"
  end

  url "https://api.bitbucket.org/2.0/repositories/mathildetech/alauda-console/downloads/alauda-console-#{
        PLATFORM
      }-#{version}",
      using: BitbucketDownloadStrategy

  if OS.mac?
    sha256 "90bfded682a3efa9f679aa78753c678101bee6ce493afcdb23b967704e7b9af3"
  elsif OS.linux?
    sha256 "7b85efafdf256a396327d6ef2ee1cf05d92721c10d0364f95823b63ce6f39f13"
  end

  def install
    mkdir_p "#{libexec}/bin"
    File.rename "./alauda-console-#{PLATFORM}-#{version}",
                "#{libexec}/bin/console"
    chmod "+x", "#{libexec}/bin/console"
    bin.install_symlink Dir["#{libexec}/bin/*"]
  end

  test { system "#{bin}/console", "-V" }
end
