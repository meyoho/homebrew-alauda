const baseConfig = require('@1stg/prettier-config')

module.exports = Object.assign({}, baseConfig, {
  overrides: baseConfig.overrides.concat({
    files: '*.rb',
    options: {
      preferSingleQuotes: false,
    },
  }),
})
