# Homebrew-Alauda

> Alauda Internal Homebrew Formulae and Casks

## What and Why

[Homebrew][] is `🍺 The missing package manager for macOS (or Linux)`.

We have or will have some cli tools for internal usage, and do want to manage them seamlessly what means installation and upgrade should be very easy and comfortable for end users. And [Homebrew][] stands behind us.

## Usage

Let's take `console-cli` as an example.

You'll need to generate an OAuth user provided by `Bitbucket` at `Bitbucket Settings` page:

![](assets/settings.jpg)

![](assets/oauth.jpg)

The callback url is required: `https://bitbucket.org`

Then you need to provide generated client_id and client_secret as two environments `HOMEBREW_CLIENT_ID` and `HOMEBREW_CLIENT_SECRET` for authorization during installation.

```sh
export HOMEBREW_CLIENT_ID=#paste your client_id here
export HOMEBREW_CLIENT_SECRET= #paste your client_secret here
```

OK, you're almost finished. Let's install the cli finally:

```sh
brew tap mathildetech/alauda https://bitbucket.org/mathildetech/homebrew-alauda
brew install console-cli
```

You can check whether the cli is working as expected by: `console -V`.

[homebrew]: https://brew.sh
