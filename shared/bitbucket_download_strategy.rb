require "download_strategy"

class BitbucketDownloadStrategy < CurlDownloadStrategy
  require "net/http"
  require "uri"
  require "json"

  uri = URI("https://bitbucket.org/site/oauth2/access_token")
  req = Net::HTTP::Post.new(uri)
  req.basic_auth(
    "#{ENV["HOMEBREW_CLIENT_ID"]}",
    "#{ENV["HOMEBREW_CLIENT_SECRET"]}"
  )
  req.set_form_data "grant_type" => "client_credentials",
                    "scopes" => "repository"
  res =
    Net::HTTP.start(
      uri.host,
      uri.port,
      use_ssl: uri.scheme == "https"
    ) { |http| http.request(req) }

  ACCESS_TOKEN = JSON.parse(res.body)["access_token"]

  private

  def _fetch(url:, resolved_url:)
    curl_download "#{resolved_url}?access_token=#{ACCESS_TOKEN}",
                  to: temporary_path
  end
end
